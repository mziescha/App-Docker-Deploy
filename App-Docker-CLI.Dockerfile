FROM mziescha/app-docker

MAINTAINER Mario Zieschang <mziescha@cpan.org>

COPY        app-docker-cli/Makefile.PL /app-docker-cli/Makefile.PL
COPY        app-docker-cli/lib/App/Docker/CLI.pm /app-docker-cli/lib/App/Docker/CLI.pm
WORKDIR     /app-docker-cli
RUN         cpanm --installdeps -n . \
        &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* /app-docker-cli

COPY        app-docker-cli /app-docker-cli
RUN         perl Makefile.PL \
        &&  make test RELEASE_TESTING=1 \
        &&  cover -test \
        &&  make install \
        &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* /app-docker-cli
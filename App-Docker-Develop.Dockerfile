FROM mziescha/app-docker-base

MAINTAINER Mario Zieschang <mziescha@cpan.org>

RUN         cpanm \
				# App::Docker::Client
		        URI \
		        Carp \
		        HTTP::Request \
		        HTTP::Lite \
		        JSON \
		        LWP::UserAgent \
		        LWP::Protocol::https \
		        LWP::Protocol::http::SocketUnixAlt \
		        AnyEvent \
		        AnyEvent::Socket \
		        AnyEvent::HTTP \
		        # App::Docker
		        DateTime::Format::Docker \
		        Acme::NameGen::CPAN::Authors \
		        Ref::Util \
		        # App::Docker::CLI
		        Tree::Simple::View \
		        Getopt::Long::Subcommand \
		        Term::ANSITable \
        &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* /app-docker-client

FROM ubuntu:16.04

# docker create --name app_docker_volumes -v "$(pwd)":/app -v ~/certs:/certs -v ~/App-Docker-Client/lib:/client_lib busybox
# perl -I /client_lib pdocker ps --remote-host <host>:<port> --tls ca.pem,cert.pem,key.pem
#
# docker build -t mziescha/app-docker App-Docker
# docker run -it --rm -v $(pwd)/App-Docker:/App-Docker -v $(pwd)/App-Docker-CLI:/App-Docker-CLI mziescha/app-docker bash -c 'perl Makefile.PL && make dist && mv /App-Docker/*.tar.gz /App-Docker-CLI/ && make veryclean'
# docker build -t mziescha/app-docker-cli App-Docker-CLI

MAINTAINER Mario Zieschang <mziescha@cpan.org>

#deps app-docker
RUN     apt-get update && apt-get install -y perl g++ curl make libssl-dev \
    &&  curl -L https://cpanmin.us | perl - App::cpanminus \
    &&  cpanm \
            DDP \
            Test::CheckManifest \
            Test::Pod::Coverage \
            Test::Pod \
            Devel::Cover \
            Pod::Coverage::TrustPod \
    &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/*

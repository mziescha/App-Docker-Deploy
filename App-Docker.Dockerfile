FROM mziescha/app-docker-client

MAINTAINER Mario Zieschang <mziescha@cpan.org>

COPY        app-docker/Makefile.PL /app-docker/Makefile.PL
COPY        app-docker/lib/App/Docker.pm /app-docker/lib/App/Docker.pm
WORKDIR     /app-docker
RUN         cpanm --installdeps -n . \
        &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* /app-docker

COPY        app-docker /app-docker
WORKDIR     /app-docker
RUN         perl Makefile.PL \
        &&  make test RELEASE_TESTING=1 \
        &&  cover -test \
        &&  make install \
        &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* /app-docker
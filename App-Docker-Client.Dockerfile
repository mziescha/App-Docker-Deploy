FROM mziescha/app-docker-base

MAINTAINER Mario Zieschang <mziescha@cpan.org>

COPY        app-docker-client/Makefile.PL /app-docker-client/Makefile.PL
COPY        app-docker-client/lib/App/Docker/Client.pm /app-docker-client/lib/App/Docker/Client.pm
WORKDIR     /app-docker-client
RUN         cpanm --installdeps . \
        &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* /app-docker-client

COPY        app-docker-client /app-docker-client
WORKDIR     /app-docker-client
RUN         perl Makefile.PL \
        &&  make test RELEASE_TESTING=1 \
        &&  cover -test \
        &&  make install \
        &&  rm -rf  /tmp/* /var/tmp/* /root/.cpanm/* /usr/share/man/* /usr/local/share/man/* /app-docker-client